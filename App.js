import React from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import HomeScreen from './screen/home';
import Sound from './screen/Sound';
import firebase from 'firebase';

const AppNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Sound: Sound
  },
  {
    initialRouteName: "Home"
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  componentWillMount(){
    var firebaseConfig = {
      apiKey: "AIzaSyDWnX7A-tVUqibv2b5qjKxWK3XxkDwb7SA",
      authDomain: "anecha4545.firebaseapp.com",
      databaseURL: "https://anecha4545.firebaseio.com",
      projectId: "anecha4545",
      storageBucket: "anecha4545.appspot.com",
      messagingSenderId: "163035631174",
      appId: "1:163035631174:web:ac9860ade111d770"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }
  render() {
    return (
      <AppContainer />
    )
  }
}


