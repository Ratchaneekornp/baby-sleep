import React from 'react';
import { ImageBackground, TextInput, ScrollView, Image, StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Baby Sleep',
    headerStyle: {
      backgroundColor: "#abd9d9"
    },
    headerTintColor: "gray"

  };
  render() {
    // let pic1 = { uri: 'https://i.ytimg.com/vi/_SXdMYghPLw/hqdefault.jpg' }
    // let pic2 = { uri: 'https://i.ytimg.com/vi/7xqwqD0c0Ls/hqdefault.jpg' }
    // let pic3 = { uri: 'https://i.ytimg.com/vi/T34PsWC-amM/hqdefault.jpg' }
    //let pic1 = { uri: '//www.maternityglow.com/wp-content/uploads/2017/04/rsz_baby-sleep-02.png' }
    return (
      // <ScrollView style={{ backgroundColor: '#b2ffff' }}>
          <ImageBackground source={{uri: 'http://www.jwwalls.com/system/product/image/1193/extra_large_famous_20clouds.jpg?nocache=1350923615.0'}} style={{width: '100%', height: '100%'}}>
        <View style={styles.container}>
          <Image style={styles.logo} source={require("./../assets/imagee/logobaby.png")} />
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Sound')}>
              <Text style={styles.button}>Go to sleep</Text>
            </TouchableOpacity>
        </View>
        </ImageBackground>
      // </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#96b2d6',
    borderColor: '#B2FFFF',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign: 'center',
    width: 150,
    height: 45,
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 160,
  },
  container1: {

    justifyContent: 'center',
    alignItems: 'center',
  },

  logo: {
    width: 250,
    height: 250
  },

});


