import React from 'react';
import { Text, View} from 'react-native';



export default class Bigboss extends React.Component {
    
  render() {

       return (
       
         <View style = {{flex:1, flexDirection:'row',borderWidth: 0.5,borderColor: 'gray',marginBottom:5}}>
            
            <View style = {{flex:2}}>
            <Text style = {{color:'#a3882e',fontSize : 18 , fontWeight : 'bold'}}>Name: {this.props.name} </Text>
            <Text style={{color:'#f2f2f7',fontSize : 18}}>Table number : {this.props.table}</Text>
            <Text style={{color:'white',fontSize:14}}>{this.props.tel}</Text>
            </View> 
         
         </View>
        
       );
    }
  }
  