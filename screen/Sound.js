import React, { Component } from "react";
import {
  ScrollView,
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Button
} from "react-native";
import { Audio } from "expo";

export default class Sound extends Component {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: "#abd9d9"
    },
    headerTintColor: "white",
    headerTitle: "White Noise"
  };

  constructor(props) {
    super(props);
    this.soundObject = new Audio.Sound();
    this.state = {
      playTime: "infinite",
      clearId: null
    };
  }

  componentWillUnmount() {
    this.clearTimeout();
  }

  selectPlayTime(type) {
    this.clearTimeout();
    this.setState({ playTime: type }, () => {
      this.setTimeOut();
    });
  }

  async playSound(songname) {
    try {
      this.clearTimeout();
      if (songname === 1) {
        await this.soundObject.loadAsync(
          require("./../assets/sounds/songbird.mp3")
        );
      } else if (songname === 2) {
        await this.soundObject.loadAsync(
          require("./../assets/sounds/HairDryer.mp3")
        );
      } else if (songname === 3) {
        await this.soundObject.loadAsync(
          require("./../assets/sounds/Pianosound.mp3")
        );
      } else if (songname === 4) {
        await this.soundObject.loadAsync(
          require("./../assets/sounds/Lowflying.mp3")
        );
      } else if (songname === 5) {
        await this.soundObject.loadAsync(
          require("./../assets/sounds/Rain_Heavy_Loud.mp3")
        );
      } else if (songname === 6) {
        await this.soundObject.loadAsync(
          require("./../assets/sounds/RelaxingOcean.mp3")
        );
      } else if (songname === 9) {
        await this.soundObject.loadAsync(
          require("./../assets/sounds/waterfall.mp3")
        );
      } else if (songname === 8) {
        await this.soundObject.loadAsync(
          require("./../assets/sounds/violin.mp3")
        );
      } else if (songname === 7) {
        await this.soundObject.loadAsync(
          require("./../assets/sounds/Vacuum.mp3")
        );
      }
      await this.soundObject.setIsLoopingAsync(true);
      await this.soundObject.playAsync();
      this.setTimeOut();
    } catch (err) {
      console.warn(err);
    }
  }

  async stopSound() {
    try {
      this.clearTimeout();
      await this.soundObject.unloadAsync();
    } catch (err) {
      console.warn(err);
    }
  }

  clearTimeout() {
    if (this.state.clearId) {
      clearTimeout(this.state.clearId);
      this.setState({ clearId: null });
    }
  }

  setTimeOut() {
    switch (this.state.playTime) {
      case "15min":
        var clearId = setTimeout(async () => {
          await this.soundObject.unloadAsync();
        }, 900000);
        this.setState({ clearId });
        break;
      case "30min":
        var clearId = setTimeout(async () => {
          await this.soundObject.unloadAsync();
        }, 1800000);
        this.setState({ clearId });
        break;
      case "1hour":
        var clearId = setTimeout(async () => {
          await this.soundObject.unloadAsync();
        }, 3600000);
        this.setState({ clearId });
        break;
    }
  }

  render() {
    return (
      <ScrollView style={{ backgroundColor: "#abd9e9" }}>
        <View style={styles.container}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ width: 100, height: 150 }}>
              <TouchableOpacity onPress={() => this.playSound(1)}>
                <Image
                  style={styles.logo}
                  onp
                  source={require("./../assets/imagee/bird.png")}
                />
              </TouchableOpacity>
            </View>
            <View style={{ width: 100, height: 150 }}>
              <TouchableOpacity onPress={() => this.playSound(2)}>
                <Image
                  style={styles.logo}
                  onp
                  source={require("./../assets/imagee/H.png")}
                />
              </TouchableOpacity>
            </View>
            <View style={{ width: 85, height: 150 }}>
              <TouchableOpacity onPress={() => this.playSound(3)}>
                <Image
                  style={styles.logo}
                  onp
                  source={require("./../assets/imagee/piano.png")}
                />
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ width: 100, height: 150 }}>
              <TouchableOpacity onPress={() => this.playSound(4)}>
                <Image
                  style={styles.logo}
                  onp
                  source={require("./../assets/imagee/plane.png")}
                />
              </TouchableOpacity>
            </View>

            <View style={{ width: 100, height: 150 }}>
              <TouchableOpacity onPress={() => this.playSound(5)}>
                <Image
                  style={styles.logo}
                  onp
                  source={require("./../assets/imagee/rain.png")}
                />
              </TouchableOpacity>
            </View>
            <View style={{ width: 85, height: 150 }}>
              <TouchableOpacity onPress={() => this.playSound(6)}>
                <Image
                  style={styles.logo}
                  onp
                  source={require("./../assets/imagee/sea.png")}
                />
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ width: 100, height: 90 }}>
              <TouchableOpacity onPress={() => this.playSound(7)}>
                <Image
                  style={styles.logo}
                  onp
                  source={require("./../assets/imagee/vacuum.png")}
                />
              </TouchableOpacity>
            </View>

            <View style={{ width: 100, height: 90 }}>
              <TouchableOpacity onPress={() => this.playSound(8)}>
                <Image
                  style={styles.logo}
                  onp
                  source={require("./../assets/imagee/violin.png")}
                />
              </TouchableOpacity>
            </View>

            <View style={{ width: 85, height: 90 }}>
              <TouchableOpacity onPress={() => this.playSound(9)}>
                <Image
                  style={styles.logo}
                  onp
                  source={require("./../assets/imagee/waterfall.png")}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View style={styles.containerStop}>
          <View
            style={{
              height: 45,
              width: 260,
              flexDirection: "row"
            }}
          >
            <TouchableOpacity
              style={
                this.state.playTime === "infinite"
                  ? styles.timeButtonActive
                  : styles.timeButton
              }
              onPress={() => {
                this.selectPlayTime("infinite");
                this.clearTimeout();
              }}
            >
              <Text style={{ color: "white" }}>Infinite</Text>
            </TouchableOpacity>
            <View style={{ padding: 5 }} />
            <TouchableOpacity
              style={
                this.state.playTime === "15min"
                  ? styles.timeButtonActive
                  : styles.timeButton
              }
              onPress={() => this.selectPlayTime("15min")}
            >
              <Text style={{ color: "white" }}>15 Min</Text>
            </TouchableOpacity>
            <View style={{ padding: 5 }} />
            <TouchableOpacity
              style={
                this.state.playTime === "30min"
                  ? styles.timeButtonActive
                  : styles.timeButton
              }
              onPress={() => this.selectPlayTime("30min")}
            >
              <Text style={{ color: "white" }}>30 Min</Text>
            </TouchableOpacity>
            <View style={{ padding: 5 }} />
            <TouchableOpacity
              style={
                this.state.playTime === "1hour"
                  ? styles.timeButtonActive
                  : styles.timeButton
              }
              onPress={() => this.selectPlayTime("1hour")}
            >
              <Text style={{ color: "white" }}>1 Hour</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={{ padding: 5 }} />

        <View style={styles.containerStop}>
          <TouchableOpacity onPress={() => this.stopSound()}>
            <Text style={styles.button}>Stop</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: "rgb(232,74,95)",
    borderColor: "rgb(232,74,95)",
    borderWidth: 1,
    borderRadius: 12,
    color: "white",
    fontSize: 16,
    // fontWeight: 'bold',
    overflow: "hidden",
    padding: 12,
    textAlign: "center",
    width: 260,
    height: 45
  },
  container: {
    justifyContent: "center",
    alignItems: "center",
    padding: 40
  },
  containerStop: {
    justifyContent: "center",
    alignItems: "center"
  },

  logo: {
    width: 80,
    height: 80,
    marginTop: 2
  },
  timeButton: {
    flex: 1,
    backgroundColor: "purple",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 12
  },
  timeButtonActive: {
    flex: 1,
    backgroundColor: "green",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 12
  }
});
