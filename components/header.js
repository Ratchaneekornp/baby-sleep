import React from 'react';
import { Text, View } from 'react-native';

export default class HeaderBar extends React.Component {
    render() {
        return (
            <View style={{
                // marginTop: 20,
                backgroundColor: 'rgba(0,0,0,200)',
            }}>
                <Text style={{
                    fontSize: 30,
                    textAlign: 'center',
                    padding: 5,
                    color: '#f2f2f7'
                }}>
                    {this.props.headtitle}
                </Text>

                {/* <Text style={{
                    container: {
                        justifyContent: 'center', 
                        alignItems: 'center' 
                  },
                }}>  
                </Text> */}
            </View>
        );
    }
}


