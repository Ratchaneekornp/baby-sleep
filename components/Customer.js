import React from 'react';
import { Text, View} from 'react-native';

export default class Customer extends React.Component {
    
  render() {

       return (
       
         <View style = {{flex:1, flexDirection:'row',borderWidth: 0.5,borderColor: 'Green',marginBottom:5}}>
            
            <View style = {{flex:2}}>
            {<Text style = {{color:'white',fontSize : 28 , fontWeight : 'bold'}}>Correct Reserve: {this.props.name} </Text> }
            <Text style={{color:'#a3882e',fontSize : 28}}>Table number : {this.props.table}</Text>
            <Text style={{fontSize:14}}>{this.props.tel}</Text>
            </View> 
         
         </View>
        
       );
    }
  }